package g2048.gamerules;
import g2048.ui.events.ChangeEvent;
import g2048.ui.events.EventType;
import g2048.ui.events.ChangeEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Game2048 implements G2048{
    private int [][] board;
    private int [][] marked;
    private int available;
    private int score;
    private int maxCell;
    private final int size = 4;
    private final List<ChangeEventListener> listeners;
    private final ChangeEvent changeEvent;
    public Game2048(int[][] board) {
        this.changeEvent = new ChangeEvent(this);
        this.board = board;
        this.available = size*size;
        updateAvailable();
        this.score = 0;
        this.listeners = new ArrayList<>();
    }

    /*public Game2048(Game2048 o){
        this.board = o.board;
    }*/
    public Game2048(){
        this.changeEvent = new ChangeEvent(this);
        this.board = new int[size][size];
        this.available = size*size;
        this.score = 0;
        this.maxCell = 0;
        placeRandomNumber();
        this.listeners = new ArrayList<>();
    }

    public void moveUp(){
        int [][] copied = copyOf();
        displacement();
        if(!Arrays.deepEquals(this.board, copied)){
            placeRandomNumber();
        }
        triggerEvent(EventType.UP);
    }
    public void moveDown(){
        int [][] copied = copyOf();
        rotate();
        rotate();
        displacement();
        rotate();
        rotate();
        if(!Arrays.deepEquals(this.board, copied)){
            placeRandomNumber();
        }
        triggerEvent(EventType.DOWN);
    }
    public void moveLeft(){
        int [][] copied = copyOf();
        rotate();
        displacement();
        rotate();
        rotate();
        rotate();
        if(!Arrays.deepEquals(this.board, copied)){
            placeRandomNumber();
        }
        triggerEvent(EventType.LEFT);
    }
    public void moveRight(){
        int [][] copied = copyOf();
        rotate();
        rotate();
        rotate();
        displacement();
        rotate();
        if(!Arrays.deepEquals(this.board, copied)){
            placeRandomNumber();
        }
        triggerEvent(EventType.RIGHT);
    }
    public boolean winGame(){
        if(this.maxCell == 8){
            triggerEvent(EventType.WIN);
        }
        return this.maxCell == 8;
    }
    public boolean lostGame(){
        boolean ans = false;
        int [][] copied = copyOf();
        Game2048 lastB = new Game2048(copied);
        lastB.moveUp();
        lastB.moveDown();
        lastB.moveRight();
        lastB.moveLeft();
        if(this.available<=0 && this.equals(lastB)){
            ans = true;
            triggerEvent(EventType.LOST);
        }
        return ans;
    }
    private void updateAvailable(){
        for (int i=0; i<this.size; i++){
            for(int j=0; j<this.size; j++){
                if(this.board[i][j] != 0){
                    this.available--;
                }
            }
        }
    }
    private int[][] copyOf(){
        int [][] ans = new int[this.size][this.size];
        for (int i=0; i<this.size; i++){
            System.arraycopy(this.board[i], 0, ans[i], 0, this.size);
        }
        return ans;
    }
    private void placeRandomNumber(){
        if(this.available > 0) {
            if(this.available==1){
                for (int i=0; i<this.size; i++){
                    for (int j=0; j<this.size; j++){
                        if(this.board[i][j] == 0){
                            this.board[i][j] = 2;
                            this.available--;
                        }
                    }
                }
            }else{
                int i = (int) (Math.random() * size);
                int j = (int) (Math.random() * size);
                while (this.board[i][j] != 0) {
                    i = (int) (Math.random() * size);
                    j = (int) (Math.random() * size);
                }
                this.board[i][j] = 2;
                this.available--;
            }
        }
    }

    public void move(char c){
        if(c == 'w') {
            displacement();
        }
        if(c == 's') {
            rotate();
            rotate();
            displacement();
            rotate();
            rotate();
        }
        if(c == 'a'){
            rotate();
            displacement();
            rotate();
            rotate();
            rotate();
        }
        if(c == 'd'){
            rotate();
            rotate();
            rotate();
            displacement();
            rotate();
        }
    }

    private void displacement() {
        this.marked = new int[size][size];
        for (int i=1; i<size; i++){
            for (int j=0; j<size;j++){
                if (this.board[i][j] != 0){
                    int ax = i;
                    int ac = this.board[i][j];
                    while(ax-1>=0 && (this.board[ax-1][j] == 0 || this.board[ax-1][j] == ac)){
                        if (this.board[ax-1][j] == 0){
                            this.board[ax-1][j] = this.board[ax][j];
                            this.board[ax][j] = 0;
                        }else{
                            addEquals(ax,ac,j);
                        }
                        ax--;
                    }
                }
            }
        }
    }
    private void addEquals(int ax, int ac, int j){
        if(this.marked[ax-1][j] != -1){
            this.board[ax-1][j] += ac;
            this.score += this.board[ax-1][j];
            this.maxCell = Math.max(this.board[ax-1][j], this.maxCell);
            this.available++;
            this.marked[ax-1][j] = -1;
            this.board[ax][j] = 0;
        }
    }
    private void rotate(){
        int [][] aux = new int[size][size];
        int i=0, j=size-1;
        while (i<size && j >= 0){
            for(int k = 0; k < size; k++){
                aux[k][j] = board[i][k];
            }
            i++;
            j--;
        }
        board = aux.clone();
    }

    public boolean equals(Object ou) {
        if(ou == null) return false;
        if(ou instanceof Game2048){
            boolean ans;
            ans = Arrays.deepEquals(this.board, ((Game2048)ou).board);
            return ans;
        }
        return false;
    }

    public String toString(){
        StringBuilder str = new StringBuilder();
        for (int i=0; i<size; i++){
            for (int j=0; j<size; j++){
                str.append(this.board[i][j]).append(" ");
            }
            str.append("\n");
        }
        return str.toString().trim();
    }

    public String gameScore() {
        return this.score+"";
    }

    @Override
    public Iterator<Iterable<Integer>> iterator() {
        return new BoardIterator(this.board);
    }

    @Override
    public void addEventListener(ChangeEventListener o) {
        this.listeners.add(o);
    }

    @Override
    public void triggerEvent(EventType event) {
        //this.changeEvent = new ChangeEvent();
        this.changeEvent.setEvent(event);
        for (ChangeEventListener listener : this.listeners){
            listener.onChange(this.changeEvent);
        }
    }
}
