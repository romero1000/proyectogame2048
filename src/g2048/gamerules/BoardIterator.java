package g2048.gamerules;
import java.util.Arrays;
import java.util.Iterator;

public class BoardIterator implements Iterator<Iterable<Integer>> {
    private final int[][] array;
    private int position;

    public BoardIterator(int [][]array){
        this.array = array;
        this.position = 0;
    }
    @Override
    public boolean hasNext() {
        return position < array.length;
    }

    @Override
    public Iterable<Integer> next() {
        position++;
        return () -> Arrays.stream(array[position - 1]).iterator();
    }
}
