package g2048.ui.events;

public interface ChangeEventListener {
    void onChange(ChangeEvent evt);
}
