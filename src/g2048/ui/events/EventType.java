package g2048.ui.events;

public enum EventType {
    WIN("You Win"), LOST("You Lost"), UP("Up"), END_GAME("Exit"), DOWN("Down"), LEFT("Left"), RIGHT("Right");

    private final String eventKey;

    EventType(String eventKey){
        this.eventKey = eventKey;
    }

    public String getName() {
        return eventKey;
    }
}