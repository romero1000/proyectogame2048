package g2048.ui.gui;
import g2048.ui.events.ChangeEvent;
import g2048.ui.events.EventType;
import g2048.gamerules.G2048;
//import g2048.ui.gui.UI2048;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;

public class GUI2048 implements KeyListener, UI2048, ActionListener{
    private final G2048 game;
    private int dimension;
    private final JLabel[] jLabels;
    private final Map<Integer, Color> mapColor;
    private JFrame mainFrame;
    private JButton upL;
    private JButton downL;
    private JButton leftL;
    private JButton rightL;
    private EventType event;
    private JButton exit_btn;

    public GUI2048(G2048 game) {
        this.game = game;
        this.event = EventType.END_GAME;
        this.game.addEventListener(this);
        size();
        mapColor = new HashMap<>();
        this.jLabels = new JLabel[this.dimension*this.dimension];
    }

    @Override
    public void play(){
        fillColors();
        initGameFrame();
        refreshView();
    }
    public void refreshView() {
        int i=0;
        JLabel j;
        for (Iterable<Integer> row : game) {
            for (int e : row) {
                j = jLabels[i];
                if (e == 0) {
                    j.setText("");
                } else if (e >= 1024) {
                    j.setFont(new Font("Element", Font.BOLD, 48));
                    j.setText(String.valueOf(e));
                } else {
                    j.setFont(new Font("Element", Font.BOLD, 50));
                    j.setText(String.valueOf(e));
                }
                j.setBackground(this.mapColor.get(e));
                i++;
            }
        }
    }

    private void fillColors(){
        this.mapColor.put(0, new Color(0xDAE0E7));
        this.mapColor.put(2, new Color(0x6FAEF7));
        this.mapColor.put(4, new Color(0x127AF2));
        this.mapColor.put(8, new Color( 0x1063C2));
        this.mapColor.put(16, new Color( 0x194B83));
        this.mapColor.put(32, new Color(0x17CECA));
        this.mapColor.put(64, new Color(0x28AF6A));
        this.mapColor.put(128, new Color(0x188412));
        this.mapColor.put(256, new Color(0xE3B51E));
        this.mapColor.put(512, new Color(0x958D5B));
        this.mapColor.put(1024, new Color(0xD25E88));
        this.mapColor.put(2048, new Color(0xB60034));
    }

    private void size(){
        int i = 0;
        for (Iterable<Integer> ignored : game){
            i++;
        }
        this.dimension = i;
    }
    private void refreshDirection(JPanel dir){
        dir.setBounds(55,500,400,100);
        dir.setLayout(new FlowLayout());
        dir.setBackground(Color.DARK_GRAY);
        this.upL = new JButton(" ↑ ");
        this.upL.setFont(new Font("Dialog", Font.BOLD, 40));
        this.downL = new JButton(" ↓ ");
        this.downL.setFont(new Font("Dialog", Font.BOLD, 40));
        this.leftL = new JButton(" ←");
        this.leftL.setFont(new Font("Dialog", Font.BOLD, 40));
        this.rightL = new JButton("→ ");
        this.rightL.setFont(new Font("Dialog", Font.BOLD, 40));
        this.exit_btn = new JButton("Salir ");
        this.exit_btn.setFont(new Font("Dialog", Font.BOLD, 20));
        updateColorLabel();
        dir.add(upL);
        dir.add(downL);
        dir.add(leftL);
        dir.add(rightL);
        dir.add(exit_btn);
        this.upL.addActionListener(this);
        this.downL.addActionListener(this);
        this.leftL.addActionListener(this);
        this.rightL.addActionListener(this);
        this.exit_btn.addActionListener(this);
    }

    private void updateColorLabel(){
        this.upL.setBackground(new Color(0xB0D1EA));
        this.upL.setOpaque(true);
        this.downL.setBackground(new Color(0xB0D1EA));
        this.downL.setOpaque(true);
        this.leftL.setBackground(new Color(0xB0D1EA));
        this.leftL.setOpaque(true);
        this.rightL.setBackground(new Color(0xB0D1EA));
        this.rightL.setOpaque(true);
    }

    private void initGameFrame() {
        this.mainFrame = new JFrame("2048 Game");
        JPanel direction = new JPanel();
        JPanel p = new JPanel();
        mainFrame.setSize(500, 700);
        mainFrame.setLayout(null);
        mainFrame.setResizable(true);
        mainFrame.getContentPane().setBackground(Color.DARK_GRAY);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.addKeyListener(this);
        //exit_btn.addActionListener(this);

        p.setBounds(55,50,400,400);
        p.setBackground(Color.gray);
        p.setLayout(new GridLayout(this.dimension,this.dimension));
        mainFrame.setFocusable(true);
        refreshDirection(direction);
        JLabel j;
        for (int i = 0; i < jLabels.length; i++) {
            jLabels[i] = new JLabel("0", JLabel.CENTER);
            j = jLabels[i];
            j.setOpaque(true);
            j.setBorder(BorderFactory.createLineBorder(new Color(0xF5000000)));
            p.add(j);
        }
        mainFrame.add(p);
        mainFrame.add(direction);
        mainFrame.setVisible(true);
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W -> game.moveUp();
            case KeyEvent.VK_S -> game.moveDown();
            case KeyEvent.VK_A -> game.moveLeft();
            case KeyEvent.VK_D -> game.moveRight();
            case KeyEvent.VK_Q -> game.triggerEvent(EventType.END_GAME);
        }
        if(game.winGame() || game.lostGame()){
            isAWinner();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public void isAWinner() {
        if (this.event == EventType.WIN){
            message("FELICIDADES USTED GANO");
            this.mainFrame.removeKeyListener(this);
            this.upL.removeActionListener(this);
            this.downL.removeActionListener(this);
            this.leftL.removeActionListener(this);
            this.rightL.removeActionListener(this);
        }
        if(this.event == EventType.LOST){
            message("LO CIENTO USTED PERDIO");
            this.mainFrame.removeKeyListener(this);
        }

    }
    public void exitGame(){
        //mainFrame.setVisible(false);
        //mainFrame.dispose();
        mainFrame.dispatchEvent(new WindowEvent(mainFrame, WindowEvent.WINDOW_CLOSING));
        //mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //mainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
    @Override
    public void onChange(ChangeEvent changeevent) {
        this.event = changeevent.getEvent();
        switch (event) {
            case UP -> {
                updateColorLabel();
                this.upL.setBackground(new Color(0x0F7D3B));
                refreshView();
            }
            case DOWN -> {
                updateColorLabel();
                this.downL.setBackground(new Color(0x0F7D3B));
                refreshView();
            }
            case LEFT -> {
                updateColorLabel();
                this.leftL.setBackground(new Color(0x0F7D3B));
                refreshView();
            }
            case RIGHT -> {
                updateColorLabel();
                this.rightL.setBackground(new Color(0x0F7D3B));
                refreshView();
            }
            case WIN, LOST -> isAWinner();
            case END_GAME -> exitGame();
        }
    }
    public void message(String txt){
        JPanel answer = new JPanel();
        answer.setBackground(Color.DARK_GRAY);
        answer.setBounds(35,2,400,100);
        answer.setLayout(new FlowLayout());
        JLabel message = new JLabel(txt);
        message.setBackground(new Color(0x81A5DC));
        message.setOpaque(true);
        message.setFont(new Font("Dialog", Font.BOLD, 20));
        answer.add(message);
        this.mainFrame.add(answer);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource() == this.leftL){
            game.moveLeft();
        }else if(e.getSource() == this.rightL){
            game.moveRight();
        }else if(e.getSource() == this.upL){
            game.moveUp();
        }else if(e.getSource() == this.downL){
            game.moveDown();
        }else if(e.getSource() == this.exit_btn){
            game.triggerEvent(EventType.END_GAME);
        }
        if(game.winGame() || game.lostGame()){
            isAWinner();
        }
    }
}
