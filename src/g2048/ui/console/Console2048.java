package g2048.ui.console;
import g2048.ui.events.ChangeEvent;
import g2048.ui.events.EventType;
import g2048.gamerules.G2048;
//import g2048.ui.console.UI2048;

import java.util.*;
//extiende evtobj
public class Console2048 implements UI2048{
    Scanner sc = new Scanner(System.in);
    private final G2048 game;
    private EventType event;

    public Console2048(G2048 game){
        this.game = game;
        this.event = EventType.UP;
        this.game.addEventListener(this);
    }

    @Override
    public void play() {
        char in;
        refreshView();
        while (true) {
            System.out.println("Comandos   arriba-->w, W, abajo-->s, S, izquierda-->a, A, derecha-->d, D salir-->q, Q");
            if(game.winGame() || game.lostGame()) break;
            in = sc.nextLine().charAt(0);
            if (((this.event == EventType.WIN) || (this.event == EventType.LOST) )) break;
            //if(game.winGame() || game.lostGame()) break;
            switch (in) {
                case 'w', 'W' -> game.moveUp();
                case 's', 'S' -> game.moveDown();
                case 'a', 'A' -> game.moveLeft();
                case 'd', 'D' -> game.moveRight();
                case 'q', 'Q' -> game.triggerEvent(EventType.END_GAME);
                default -> System.out.println("JUGADA INVALIDA POR FAVOR JUEGUE OTRA VEZ");
            }
        }
    }

    public void refreshView() {

        for (Iterable<Integer> row : game) {
            for (int j : row) {
                System.out.print(j+" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void isAWinner() {
        if (this.event == EventType.WIN){
            System.out.println("FELICIDADES GANASTE");

            /*
            char quit;
            while(true){
                System.out.println("Presione Q para salir");
                quit = sc.nextLine().charAt(0);
                switch (quit) {
                    case 'q', 'Q' -> game.triggerEvent(EventType.END_GAME);
                    default -> System.out.println("JUGADA INVALIDA POR FAVOR JUEGUE OTRA VEZ");
                }
            }*/
        }
        if(this.event == EventType.LOST){
            System.out.println("PERDISTE EL JUEGO");
        }

    }
    public void exitGame(){
        System.out.println("Se salio del juego");
    }
    @Override
    public void onChange(ChangeEvent changeevent) {
        this.event = changeevent.getEvent();
        switch (event){
            case UP, DOWN, LEFT, RIGHT -> refreshView();
            case WIN, LOST -> isAWinner();
            case END_GAME -> exitGame();
        }
    }
}
