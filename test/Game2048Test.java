import org.junit.Test;
import g2048.gamerules.Game2048;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Game2048Test {
    @Test
    public void gameStatusForWin() {
        int [][]in={{4,2,4,4},
                    {4,2,4,4},
                    {4,4,0,0},
                    {4,4,0,0}};
        Game2048 current = new Game2048(in);
        current.moveLeft();
        assertTrue(current.winGame());
    }
    @Test
    public void gameStatusForLose() {
        int [][]in={{4,2,4,2},
                    {2,4,2,4},
                    {4,2,4,2},
                    {2,4,16,4}};
        Game2048 current = new Game2048(in);
        current.moveDown();
        assertTrue(current.lostGame());
    }
    @Test
    public void availableWithOneCell() {
        int [][]in={{4,2,4,2},
                    {2,4,2,8},
                    {4,2,4,4},
                    {2,4,2,4}};
        int [][]ou={{4,2,4,2},
                    {2,4,2,2},
                    {4,2,4,8},
                    {2,4,2,8}};
        Game2048 current = new Game2048(in);
        Game2048 expected = new Game2048(ou);
        current.moveDown();
        assertEquals(expected, current);
    }
    @Test
    public void gameStatusForLoseT() {
        int [][]in={{4,2,4,2},
                    {2,4,2,4},
                    {4,2,4,2},
                    {2,4,2,4}};
        Game2048 current = new Game2048(in);
        assertTrue(current.lostGame());
    }
    @Test
    public void scoreGame() {
        int [][]in={{4,2,4,4},
                    {4,4,2,2},
                    {4,2,0,0},
                    {4,4,16,16}};
        Game2048 current = new Game2048(in);
        current.moveUp();
        assertEquals("16", current.gameScore());
    }
    /*
    @Test
    public void moveUpUsingRandom() {
        Game2048 current = new Game2048();
        Game2048 expected = new Game2048(current);
        current.move('w');
        expected.move('w');
        assertEquals(expected, current);
    }
    @Test
    public void moveDownUsingRandom() {
        Game2048 current = new Game2048();
        Game2048 expected = new Game2048(current);
        current.move('s');
        expected.move('s');
        assertEquals(expected, current);
    }*/

    @Test
    public void moveUp() {
        int [][]in={{0,2,0,0},
                    {0,2,0,0},
                    {0,4,0,2},
                    {0,0,0,0}};
        int [][]ou={{0,4,0,2},
                    {0,4,0,0},
                    {0,0,0,0},
                    {0,0,0,0}};
        Game2048 current = new Game2048(in);
        Game2048 expected = new Game2048(ou);
        current.move('w');
        assertEquals(expected, current);
    }
    @Test
    public void moveUpAndAdd() {
        int [][]in={{0,0,0,2},
                    {0,0,0,0},
                    {0,2,0,2},
                    {0,0,0,0}};
        int [][]ou={{0,2,0,4},
                    {0,0,0,0},
                    {0,0,0,0},
                    {0,0,0,0}};
        Game2048 current = new Game2048(in);
        Game2048 expected = new Game2048(ou);
        current.move('w');
        assertEquals(expected, current);
    }
    @Test
    public void moveToTheMaxAndAdd() {
        int [][]in={{0,0,0,4},
                    {0,0,0,4},
                    {0,0,2,4},
                    {0,0,4,2}};
        int [][]ou={{0,0,2,8},
                    {0,0,4,4},
                    {0,0,0,2},
                    {0,0,0,0}};
        Game2048 current = new Game2048(in);
        Game2048 expected = new Game2048(ou);
        current.move('w');
        assertEquals(expected, current);
    }
    @Test
    public void moveUpOtherCase() {
        int [][]in={{4,2,0,4},
                    {4,2,0,4},
                    {4,0,2,4},
                    {4,0,4,2}};
        int [][]ou={{8,4,2,8},
                    {8,0,4,4},
                    {0,0,0,2},
                    {0,0,0,0}};
        Game2048 current = new Game2048(in);
        Game2048 expected = new Game2048(ou);
        current.move('w');
        assertEquals(expected, current);
    }

    @Test
    public void moveDown() {
        int [][]in={{0,0,0,0},
                    {0,2,0,2},
                    {0,0,0,0},
                    {0,0,0,0}};
        int [][]ou={{0,0,0,0},
                    {0,0,0,0},
                    {0,0,0,0},
                    {0,2,0,2}};
        Game2048 current = new Game2048(in);
        Game2048 expected = new Game2048(ou);
        current.move('s');
        assertEquals(expected, current);
    }
    @Test
    public void moveDownAndAdd() {
        int [][]in={{0,0,0,0},
                    {0,2,0,2},
                    {0,0,0,0},
                    {0,0,0,2}};
        int [][]ou={{0,0,0,0},
                    {0,0,0,0},
                    {0,0,0,0},
                    {0,2,0,4}};
        Game2048 current = new Game2048(in);
        Game2048 expected = new Game2048(ou);
        current.move('s');
        assertEquals(expected, current);
    }
    @Test
    public void moveDownToTheMaxAndAdd() {
        int [][]in={{0,0,4,2},
                    {0,0,2,4},
                    {0,0,0,4},
                    {0,0,0,4}};
        int [][]ou={{0,0,0,0},
                    {0,0,0,2},
                    {0,0,4,4},
                    {0,0,2,8}};
        Game2048 current = new Game2048(in);
        Game2048 expected = new Game2048(ou);
        current.move('s');
        assertEquals(expected, current);
    }
    @Test
    public void moveDownOtherCase() {
        int [][]in={{0,0,4,2},
                    {0,0,2,4},
                    {0,0,0,4},
                    {0,0,0,4}};
        int [][]ou={{0,0,0,0},
                    {0,0,0,2},
                    {0,0,4,4},
                    {0,0,2,8}};
        Game2048 current = new Game2048(in);
        Game2048 expected = new Game2048(ou);
        current.move('s');
        assertEquals(expected, current);
    }

    @Test
    public void moveLeft() {
        int [][]in={{0,0,2,2},
                    {0,0,2,4},
                    {0,0,4,4},
                    {4,0,2,4}};
        int [][]ou={{4,0,0,0},
                    {2,4,0,0},
                    {8,0,0,0},
                    {4,2,4,0}};
        Game2048 current = new Game2048(in);
        Game2048 expected = new Game2048(ou);
        current.move('a');
        assertEquals(expected, current);
    }

    @Test
    public void moveRight() {
        int [][]in={{0,0,2,2},
                    {8,0,8,0},
                    {0,0,4,4},
                    {2,0,2,2}};
        int [][]ou={{0,0,0,4},
                    {0,0,0,16},
                    {0,0,0,8},
                    {0,0,2,4}};
        Game2048 current = new Game2048(in);
        Game2048 expected = new Game2048(ou);
        current.move('d');
        assertEquals(expected, current);
    }

}

